FROM python:3.7.3-slim-stretch

MAINTAINER concent (Python/Flask)

COPY app /app
COPY requirements.txt /

RUN pip install -r /requirements.txt 

EXPOSE 5000

CMD ["python", "/app/entrypoint.py"]
